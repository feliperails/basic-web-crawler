# Basic Web Crawler

[![Python](https://img.shields.io/badge/python-3.11-green)](https://www.python.org)
[![Black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

Basic Web Crawler extracts logo and phone numbers from a website list.

The program doesn't print anything to the terminal. Just put the list of URLs separated by break lines.

When this program detects a blank line or end of file, it prints the list with logo and phone numbers in JSON format.


#### Requirements

- Make
- Python 3.11
- Pip >= 21.0.1
- Other requirements: 
    - requirements.txt 
    - requirements-dev.txt (development dependencies)
  
#### Installing dev environment requirements:

```
make install-dev
```

#### Linting and formating code:

```
make lint
make format
```

#### Run local:

```
python -m basic_web_crawler
```

#### Run local using Docker:

```
docker build -t basic_web_crawler .
docker run -i basic_web_crawler
```
If you have a website list in a file, you can use:
```
cat websites.txt | docker run -i basic_web_crawler
```