from http import HTTPStatus

import aiohttp
import yarl
from pyquery import PyQuery
from src.element.logo import process_logo
from src.element.phone import process_phone


async def fetch(clientSession: aiohttp.ClientSession, url: str) -> dict[str, str] | None:
    headers = {
        "User-Agent": (
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 "
            "(KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36"
        )
    }
    url = yarl.URL(url, encoded=True)
    async with clientSession.get(url, headers=headers) as r:
        if not r.ok and r.status != HTTPStatus.NOT_FOUND:
            return {"logo": None, "phone": None, "website": str(url)}

        text = await r.text()

        pq = PyQuery(text)

        url_logo = process_logo(pq, str(url))

        phone = process_phone(pq)

        return {"logo": url_logo, "phone": phone, "website": str(url)}
