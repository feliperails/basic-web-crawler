import re
from pyquery import PyQuery


def process_phone(pq: PyQuery) -> list[str]:
    selector = pq("[href^='tel']")
    phones = []

    if selector:
        for item in selector:
            phone = PyQuery(item).attr("href")
            phones.append(__sanitaze_phone(phone))

        return phones

    selector = pq("b,i,u,strong,span,p")

    for item in selector:
        text = pq(item).text()
        if re.match(r"^[\d\s+()-]{7,}$", text):
            phones.append(__sanitaze_phone(text))

    return phones


def __sanitaze_phone(phone):
    phone = re.sub("[^0-9+ ]", " ", phone)
    phone = re.sub(" +", " ", phone).strip()
    return phone
