import os
import re

from pyquery import PyQuery
from urllib.parse import urljoin


def __is_image_file(path):
    image_extensions = {".jpg", ".jpeg", ".png", ".gif", ".bmp", ".png", ".webp"}

    filename, extension = os.path.splitext(path)
    return extension.lower() in image_extensions


def __calculate_area(dimensions):
    try:
        width, height = map(int, dimensions.split("x"))
        return width * height
    except ValueError:
        return None


def __sanitize_img_url(img_url: str, base_url: str) -> str:
    return urljoin(base_url, img_url)


def __resolve_logo_default(selector, base) -> str | None:
    if selector:
        if selector[0].tag == "img":
            return __sanitize_img_url(selector.attr("src"), base)
        else:
            img_selector = selector.find("img")
            if img_selector:
                return __sanitize_img_url(img_selector.attr("src"), base)

    return None


def __resolve_rel_icon(selector, base) -> str | None:
    selected_selector = None
    current_area = 0
    for item in selector:
        item_selector = PyQuery(item)
        sizes = item_selector.attr("sizes")
        area = __calculate_area(sizes) if sizes else 0
        if area > current_area:
            selected_selector = item_selector

    if not selected_selector:
        selected_selector = PyQuery(selector[0])

    return __resolve_element_href(selected_selector, base)


def __resolve_element_href(selector, base) -> str | None:
    url = __sanitize_img_url(selector.attr("href"), base)
    if __is_image_file(url):
        return url


def __resolve_element_content(selector, base) -> str | None:
    url = __sanitize_img_url(selector.attr("content"), base)
    if __is_image_file(url):
        return url


def process_logo(pq: PyQuery, url: str) -> str:
    base = pq("base").attr("href")
    if not base:
        base = url

    filters = [
        {"query": "link[rel='icon']", "function": __resolve_rel_icon},
        {"query": "link[rel='apple-touch-icon']", "function": __resolve_element_href},
        {"query": "[id='logo']:first", "function": __resolve_logo_default},
        {"query": "[class='logo']:first", "function": __resolve_logo_default},
        {"query": "header [id*='logo']:first", "function": __resolve_logo_default},
        {"query": "header [class*='logo']:first", "function": __resolve_logo_default},
        {"query": "[id*='logo']:first", "function": __resolve_logo_default},
        {"query": "[class*='logo']:first", "function": __resolve_logo_default},
        {"query": "meta[property='og:image']:first", "function": __resolve_element_content},
    ]
    for item in filters:
        selector = pq(item["query"])
        if not selector:
            continue

        result = item["function"](selector, base)
        if result:
            return result

    compiled_pattern = re.compile("logo", re.IGNORECASE)
    all_elements = pq("body *")
    for element in all_elements:
        classes = element.attrib.get("class")
        if not classes:
            continue

        if compiled_pattern.search(classes):
            return __resolve_logo_default(pq(element), base)
