import json
from src.page import fetch
import asyncio
import aiohttp


async def fetch_all(s, urls):
    tasks = []
    for url in urls:
        task = asyncio.create_task(fetch(s, url))
        tasks.append(task)
    res = await asyncio.gather(*tasks)
    return res


async def main(urls):
    async with aiohttp.ClientSession() as session:
        htmls = await fetch_all(session, urls)
        print(json.dumps(htmls, indent=2))


if __name__ == "__main__":
    lines = []
    while True:
        try:
            line = input()

            if not line.strip():
                break

            lines.append(line)
        except EOFError:
            break

    asyncio.run(main(lines))
