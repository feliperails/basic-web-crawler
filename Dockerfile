FROM python:3.11-slim

WORKDIR /app
COPY ./src /app/src
COPY basic_web_crawler.py .
COPY requirements.txt .
COPY requirements-dev.txt .
COPY Makefile .

RUN pip install --no-cache-dir -r requirements.txt

CMD ["python", "basic_web_crawler.py"]